#!/usr/bin/env bash

#    (c) 2016-2017, n0vember <n0vember@half-9.net>
#
#    This file is part of nocloud.
#
#    nocloud is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    nocloud is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with nocloud.  If not, see <http://www.gnu.org/licenses/>.

# This is a set of function for the nocloud command

##### OLD TOOLS

deprecated() {
  echo '
     | | | | | | | | | | | | | | | | | | | | |
   \                                           /
  -        This command is deprecated !         -
  -   It will be removed in a future version.   -
  -   Please use the nocloud command instead.   -
   /                                           \
     | | | | | | | | | | | | | | | | | | | | |
' >&2
}

##### INVENTORY

ansible_inventory() {
  local contextName="$1"
  local typeName="$2"
  local line host context type ip previous_context previous_type children hosts

  echo "{"

  for line in $(vm_running "${contextName}" "${typeName}" | sort -k 2 | tr " " "/")
  do

    host=$(echo "$line" | cut -d "/" -f 1)
    context=$(echo "$line" | cut -d "/" -f 2)
    if [ "${context}" == "${vmCreateContext}" ] ; then
      type=${context}
      ip=$(echo "$line" | cut -d "/" -f 3)
    else
      type=$(echo "$line" | cut -d "/" -f 3)
      ip=$(echo "$line" | cut -d "/" -f 4)
    fi

    if [ "${previous_type}" != "${type}" ] ; then
      if [ -n "${previous_type}" ] ; then
        hosts=$(echo "${hosts}" | sed 's/,$//')
        echo "  \"${previous_context}_${previous_type}\" : {"
        echo "    \"hosts\" : [ ${hosts} ]"
        echo "  },"
        hosts=""
      fi
      previous_type=${type}
    fi

    if [ "${previous_context}" != "${context}" ] ; then
      if [ -n "${previous_context}" ] ; then
        echo "  \"${previous_context}\" : {"
        children=$(echo ${children} | tr ' ' '\n' | sort | uniq | xargs echo)
        children=$(echo "${children}" | sed 's/,$//')
        echo "    \"children\" : [ ${children} ],"
        echo "    \"vars\": {"
        echo "      \"ansible_ssh_common_args\": \"-o StrictHostKeyChecking=no -o CheckHostIp=False -o UserKnownHostsFile=/dev/null\","
        echo "      \"ansible_user\": \"root\""
        echo "    }"
        echo "  },"
        children=""
      fi
      previous_context=${context}
    fi

    hosts="${hosts} \"${ip}\","
    children="${children} \\\"${context}_${type}\\\","

  done

  if [ -n "${type}" ] ; then
    hosts=$(echo "${hosts}" | sed 's/,$//')
    echo "  \"${context}_${type}\" : {"
    echo "    \"hosts\" : [ ${hosts} ]"
    echo "  },"
  fi

  if [ -n "${context}" ] ; then
    echo "  \"${context}\" : {"
    children=$(echo ${children} | tr ' ' '\n' | sort | uniq | xargs echo)
    children=$(echo "${children}" | sed 's/,$//')
    echo "    \"children\" : [ ${children} ],"
    echo "    \"vars\": {"
    echo "      \"ansible_ssh_common_args\": \"-o StrictHostKeyChecking=no -o CheckHostIp=False -o UserKnownHostsFile=/dev/null\","
    echo "      \"ansible_user\": \"root\""
    echo "    }"
    echo "  },"
  fi

  echo "  \"_meta\": {"
  echo "    \"hostvars\": {}"
  echo "  }"

  echo "}"
}

### VM EXECUTION CONTROL

stop_all() {
  local contextName="$1"
  local vms vm
  vms=$(vm_running "${contextName}" "*" | cut -d ' ' -f 1 | xargs)
  log "My vms are currently : "
  for vm in ${vms} ; do
    log " - ${vm}"
  done
  for vm in ${vms} ; do
    log "shutting down ${vm}"
    vm_stop ${vm} ${contextName} $(vm_type ${vm})
  done
}

stop_and_keep_all() {
  log "I am being told to shut down."
  stop_all ${contextName}
  log "Good bye !"
  exit
}

stop_and_destroy_all() {
  log "I am being told to shut down and destroy all running VMs."
  stop_all ${contextName}
  log "removing files"
  vm_context_destroy ${contextName}
  log "Good bye !"
  exit
}

run_from_spec() {
  local guiOpt="$1"
  shift
  local specLine="$@"
  local spec
  for spec in ${specLine} ; do
    local vmName=$(echo "${spec}" | cut -d : -f 1)
    local vmCount=$(echo "${spec}" | cut -d : -f 2)
    local vmType=$(echo "${spec}" | cut -d : -f 3)
    local vmOpts=$(echo "${spec}" | cut -d : -f 4 | tr ";" " ")
    local vmSpawned=$(vm_instantiate ${vmName} ${vmCount} ${context} ${vmType} "${vmOpts} ${guiOpt}")
    local vm
    local vms
    for vm in $(echo "${vmSpawned}" | tr -s '\n ' '  ') ; do
      vms="${vms} ${vm}"
    done
  done
  echo "${vms}"
}

run() {
  local guiOpt
  local foregroundMode=0
  local specs specLine
  local specCount=0
  local lineIFS='
'
  local oldIFS
  local stopSignal=SIGUSR1
  local vms vm

  while [ $# -gt 0 ] ;
  do
    case "$1" in
      gui)
        guiOpt=gui
        shift
        ;;
      foreground)
        foregroundMode=1
        shift
        ;;
      machines)
        specs="${specs} $2"
        specCount=$((specCount+1))
        shift 2
        ;;
      file)
        oldIFS="${IFS}"
        IFS="${lineIFS}"
        [ -f "$2" ] || usage "$2: file does not exist"
        for specLine in $(sed '/^ *$/d;s/ *#.*$//' $2)
        do
          specs="${specs} ${specLine}"
          specCount=$((specCount+1))
        done
        IFS="${oldIFS}"
    esac
  done

  log "I will be managing the ${context} context"
  [ ${foregroundMode} -eq 1 ] || log "Background mode activated: I will quit when my VMs are up."

  trap 'stop_and_destroy_all' SIGINT SIGTERM
  trap 'stop_and_keep_all' ${stopSignal}

  # search for running vms in context
  log "searching for running machines in context"
  vms=$(vm_running "${context}" "*" | cut -d ' ' -f 1 | xargs)
  if [ -n "${vms}" ] ; then
    log "${context} is not empty !"
    for vm in ${vms} ; do
      log "${vm} is of $(vm_type ${vm}) type in $(vm_context ${vm}) context"
    done
  fi

  # run vms specified on command line
  log "running new machines"
  for vm in $(run_from_spec "${guiOpt}" ${specs})
  do
    log "${vm} of $(vm_type ${vm}) type has been spawned in $(vm_context ${vm}) context"
    vms="${vms} ${vm}"
  done

  # search for existing vms in context
  log "searching for stopped machine in context"
  contexts=$(ls ${vmBaseDir} | grep -v "${vmCreateContext}")
  echo ${contexts} | tr ' ' '\n' | grep "^${context}$" >/dev/null || usage "${context} is no known context"
  types=$(ls ${vmBaseDir}/${context})
  [ -n "${types}" ] || usage "no type for context ${context}"
  for type in ${types}
  do
    for vm in $(ls ${vmBaseDir}/${context}/${type})
    do
      if echo "${vms}" | tr ' ' '\n' | grep "^${vm}$" >/dev/null 2>&1 ; then
        true
      else
        vm_run ${vm} ${context} ${type} none ${guiOpt}
        log "${vm} of $(vm_type ${vm}) type has been spawned in ${context} context"
        vms="${vms} ${vm}"
      fi
    done
  done

  # loop
  local vmWithoutIp
  local vmsWithoutIp=$(echo "${vms}" | sed -r 's/^ *(.*) *$/\1/')
  while [ -n "${vms}" ] ; do
    local loopOperations=0
    # wait for all vms to have an IP
    if [ -n "${vmsWithoutIp}" ] ; then
      for vmWithoutIp in ${vmsWithoutIp} ; do
        ip=$(vm_running "${context}" "*" | grep ${vmWithoutIp} | cut -d ' ' -f 3 )
        if [ -n "${ip}" ] ; then
          log "${vmWithoutIp} has IP ${ip}"
          vmsWithoutIp=$(echo "${vmsWithoutIp}" | sed "s/ *${vmWithoutIp} */ /" | sed -r 's/^ *(.*) *$/\1/')
          log "${vmWithoutIp} has console socket $(vm_console_socket ${vmWithoutIp})"
          log "${vmWithoutIp} has spice port $(vm_spice_port ${vmWithoutIp})"
          loopOperations=$((loopOperations+1))
        fi
      done
    # exit if background mode
    elif [ ${foregroundMode} -eq 0 ] ; then
      log "All of my machines are running."
      log "I quit."
      break
    fi
    # check for disapearing VMs
    for vm in ${vms} ; do
      if [ -z "$(vm_running "${context}" "*" | grep ${vm})" ] ; then
        log "${vm} has vanished..."
        vms=$(echo "${vms}" | sed "s/ *${vm} */ /;s/^ *//;s/ *$//")
        loopOperations=$((loopOperations+1))
      fi
    done
    # check for appearing VMs
    for vm in $(vm_running "${context}" "*" | cut -d ' ' -f 1) ; do
      if [ $(echo "${vms}" | tr -s ' ' '\n' | grep ${vm} | wc -l) -eq 0 ] ; then
        log "${vm} has joined the context from outer space."
        vms="${vms} ${vm}"
        vmsWithoutIp="${vmsWithoutIp} ${vm}"
        loopOperations=$((loopOperations+1))
      fi
    done
    [ ${loopOperations} -eq 0 ] && sleep 1
  done
  # end when all machines gone (foreground mode)
  if [ ${foregroundMode} -eq 1 ] ; then
    log "All of my machines have gone."
    log "I quit."
  fi

  trap '-' SIGINT SIGTERM
  trap '-' ${stopSignal}
}

### VM MANAGEMENT

console() {
  local vmName=$1
  [ -z ${vmName} ] && usage "VM name is mandatory"
  vm_running "${context}" "*" | grep "^${vmName} " >/dev/null || usage "no such machine in your context"
  local port=$(vm_spice_port "${vmName}")
  which spicy >/dev/null 2>&1 || usage "spicy is needed for this feature"
  spicy -h localhost -p $port -w ${vmSpicePassword} --class=qemu-system-${vmSystemType} >/dev/null 2>&1 &
}

manage() {
  local vmName=$1
  [ -z ${vmName} ] && usage "VM name is mandatory"
  vm_running "${context}" "*" | grep "^${vmName} " >/dev/null || usage "no such machine in your context"
  shift
  if [ $# -gt 0 ] ; then
    local deviceGiven=$1
    local deviceName
    case $deviceGiven in
      iso) deviceName=ide1-cd0 ;;
      floppy) deviceName=floppy0 ;;
      *) usage "optional arguments to qemu command are iso or floppy" ;;
    esac
    local fileName=$2
    [ -z "$fileName" ] && usage "no ${deviceGiven} name provided"
    if [ "${fileName}" == "none" ] ; then
      vm_console_command ${vmName} eject ${deviceName}
    else
      [ -f "${fileName}" ] || usage "${deviceGiven} file does not exists"
      vm_console_command ${vmName} change ${deviceName} "${fileName}"
    fi
  else
    vm_console_command ${vmName}
  fi
}

### SNAPSHOTS

vm_take_snapshot_when_stopped() {
  local vmName=$1
  local contextName=$2
  local typeName=$3

  local vmDisks=$(vm_disks ${vmName} ${contextName} ${typeName} | cut -d ' ' -f 1)
  local vmDir=$(vm_dir ${vmName} ${contextName} ${typeName})
  local snapTag="vd-$(date +%Y%m%d%H%M%S)"
  local vmDisk
  for vmDisk in ${vmDisks}
  do
    qemu-img snapshot -c ${snapTag} ${vmDir}/${vmDisk}
  done
}

vm_take_snapshot_when_started() {
  local vmName=$1
  local contextName=$2
  local typeName=$3

  vm_console_command ${vmName} savevm
}

vm_list_snapshots_when_stopped() {
  local vmName=$1
  local contextName=$2
  local typeName=$3

  local vmMainDisk=$(vm_disks ${vmName} ${contextName} ${typeName} | cut -d ' ' -f 1)
  local vmDir=$(vm_dir ${vmName} ${contextName} ${typeName})
  qemu-img snapshot -l ${vmDir}/${vmMainDisk}
}

vm_list_snapshots_when_started() {
  local vmName=$1
  local contextName=$2
  local typeName=$3

  vm_console_command ${vmName} info snapshots
}

vm_restore_started_snapshot() {
  local snapTag=$1
  local vmName=$2
  local contextName=$3
  local typeName=$4

  vm_run ${vmName} ${contextName} ${typeName}
  vm_console_command ${vmName} loadvm ${snapTag} >/dev/null 2>&1
}

vm_restore_stopped_snapshot() {
  local snapTag=$1
  local vmName=$2
  local contextName=$3
  local typeName=$4

  local vmDisks=$(vm_disks ${vmName} ${contextName} ${typeName} | cut -d ' ' -f 1)
  local vmDir=$(vm_dir ${vmName} ${contextName} ${typeName})
  local vmDisk
  for vmDisk in ${vmDisks}
  do
    qemu-img snapshot -a ${snapTag} ${vmDir}/${vmDisk}
  done
}

vm_delete_snapshot_when_started() {
  local snapTag=$1
  local vmName=$2
  local contextName=$3
  local typeName=$4

  vm_console_command ${vmName} delvm ${snapTag}
}

vm_delete_snapshot_when_stopped() {
  local snapTag=$1
  local vmName=$2
  local contextName=$3
  local typeName=$4

  local vmDisks=$(vm_disks ${vmName} ${contextName} ${typeName} | cut -d ' ' -f 1)
  local vmDir=$(vm_dir ${vmName} ${contextName} ${typeName})
  local vmDisk
  for vmDisk in ${vmDisks}
  do
    qemu-img snapshot -d ${snapTag} ${vmDir}/${vmDisk}
  done
}

vm_snapshot_manage() {
  local contextName=$1
  shift

  local modeName=$1
  local vmName=$2
  local tagCmd=$3
  local tagName=$4
  local typeName="*"
  local contextName

  [ -z ${modeName} ] && usage "action on snapshot is mandatory"
  [ -z ${vmName} ] && usage "VM name is mandatory"
  [ "${contextName}" == "*" ] && contextName=$(vm_context ${vmName})
  [ "${typeName}" == "*" ] && typeName=$(vm_type ${vmName})
  local vmRunning=$(vm_running "${contextName}" "${typeName}" | grep "^${vmName}" | wc -l)

  case ${modeName} in
    "take")
      [ $# -ne 2 ] && usage "illegal number of arguments"
      if [ ${vmRunning} -gt 0 ] ; then
        vm_take_snapshot_when_started ${vmName} "${context}" "${typeName}"
      else
        vm_take_snapshot_when_stopped ${vmName} "${context}" "${typeName}"
      fi
      ;;
    "list")
      [ $# -ne 2 ] && usage "illegal number of arguments"
      if [ ${vmRunning} -gt 0 ] ; then
        vm_list_snapshots_when_started ${vmName} "${context}" "${typeName}"
      else
        vm_list_snapshots_when_stopped ${vmName} "${context}" "${typeName}"
      fi
      ;;
    "restore")
      [ $# -ne 4 ] && usage "illegal number of arguments"
      [ -z ${tagName} ] && usage "snapshot tag is mandatory"
      local snapType=unknown
      echo ${tagName} | grep "^vm-" >/dev/null && snapType=started
      echo ${tagName} | grep "^vd-" >/dev/null && snapType=stopped
      [ ${vmRunning} -gt 0 ] && vm_stop ${vmName} ${contextName} ${typeName}
      [ ${snapType} == "started" ] && vm_restore_started_snapshot ${tagName} ${vmName} "${context}" "${typeName}"
      [ ${snapType} == "stopped" ] && vm_restore_stopped_snapshot ${tagName} ${vmName} "${context}" "${typeName}"
      [ ${snapType} == "unknown" ] && error "I don't know how to handle this snapshot"
      ;;
    "delete")
      [ $# -ne 4 ] && usage "illegal number of arguments"
      [ -z ${tagName} ] && usage "snapshot tag is mandatory"
      if [ ${vmRunning} -gt 0 ] ; then
        vm_delete_snapshot_when_started ${tagName} ${vmName} "${context}" "${typeName}"
      else
        vm_delete_snapshot_when_stopped ${tagName} ${vmName} "${context}" "${typeName}"
      fi
      ;;
    *)
      usage "invalid action on snapshot"
      ;;
  esac
}

### templates management

template_download() {
  local vmTemplate="$1"

  vmTemplateDir=$(vm_dir ${vmTemplate} ${vmCreateContext})
  if [ -d "${vmTemplateDir}" ] ; then
    log_error "you already have a template called ${vmTemplate}"
    exit 1
  fi

  trap "[ -f ${vmTemplateDir}/.downloaded ] || rm -rvf ${vmTemplateDir} ; exit 1" SIGINT SIGTERM

  echo ${vmTemplates} | tr " " "\n" | grep "^${vmTemplate}$" >/dev/null || usage "unknown template"
  local vmTemplateImage vmTemplateImageSum vmTemplateName vmTemplateImageDir
  vmTemplateImage=$(curl -L -s "${vmTemplatesBaseUrl}/${vmTemplate}.latest")
  vmTemplateImageSum="${vmTemplateImage}.sha256"
  vmTemplateName=$(basename ${vmTemplateImage} .img)
  vmTemplateImageDir=$(vm_dir ${vmTemplateName} ${vmCreateContext})

  mkdir -p ${vmTemplateImageDir}
  [ -L ${vmTemplateDir} ] && rm ${vmTemplateDir}
  ln -s $(basename ${vmTemplateImageDir}) ${vmTemplateDir}
  cd ${vmTemplateImageDir}
  local vmTemplateSize vmTemplateSizeMB
  vmTemplateSize=$(curl --head -s "${vmTemplatesBaseUrl}/${vmTemplateImage}" | sed '/^content-length:/!d;s/.*: //' | tr -d '\015') # remove CR from MS CR/LF
  vmTemplateSizeMB=$((vmTemplateSize/(1024*1024)))
  log "fetching ${vmTemplate} disk image (${vmTemplateSizeMB}MB, please be patient...)"
  curl -s "${vmTemplatesBaseUrl}/${vmTemplateImage}" > "${vmTemplateImage}"
  log "fetching ${vmTemplate}.img checksum"
  local vmTemplatesBaseSum
  vmTemplatesBaseSum=$(curl -s ${vmTemplatesBaseUrl}/${vmTemplateImageSum})
  cat > ${vmTemplate}.conf <<EOF
vmName=${vmTemplate}
vmMem=512
vmCpu=2
vmDisk=${vmTemplateImage}
EOF
  . ${vmTemplate}.conf

  log "checking sha256"
  local sha256
  sha256=$(cat ${vmTemplateImageDir}/${vmDisk} | sha256sum | sed 's/ .*//')
  [ "${sha256}" == "${vmTemplatesBaseSum}" ] || usage "checkum does not match (got '${sha256}' instead of '${vmTemplatesBaseSum}')"

  log "starting template"
  vm_run ${vmTemplate} ${vmCreateContext} "" "none"
  local vmIp vmSshOpen
  vmIp=$(vm_ip ${vmTemplate} | tr ' ' '\n' | sort -u | tail -1)
  vmSshOpen=$(LANG= echo 2>&1 >/dev/tcp/${vmIp}/22 | grep "Connection refused" | wc -l)
  while [ ${vmSshOpen} -ne 0 ]
  do
    sleep 1
    vmSshOpen=$(LANG= echo 2>&1 >/dev/tcp/${vmIp}/22 | grep "Connection refused" | wc -l)
  done
  log "sending public keys"
  ls ~/.ssh/*.pub >/dev/null 2>&1 || usage "you don't have ssh keys"
  local temporaryKey
  temporaryKey=$(mktemp)
  cp ${realDir}/../etc/nocloud ${temporaryKey}
  chmod 600 ${temporaryKey}
  ssh-add -L | ssh -o StrictHostKeyChecking=False -o CheckHostIp=False -o UserKnownHostsFile=/dev/null -l root -i ${temporaryKey} ${vmIp} tee -a /root/.ssh/authorized_keys >/dev/null
  rm ${temporaryKey}

  log "stopping vm"
  vm_console_command ${vmName} system_powerdown

  touch .downloaded
}
