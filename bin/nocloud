#!/usr/bin/env bash

#    (c) 2016-2017, n0vember <n0vember@half-9.net>
#
#    This file is part of nocloud.
#
#    nocloud is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    nocloud is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with nocloud.  If not, see <http://www.gnu.org/licenses/>.

usage() {
  echo "$@" >&2
  echo "usage : $(basename $0) command

  command is one of the following :

    template download TEMPLATE_NAME - download a template. TEMPLATE_NAME is one of ${vmTemplates}
    template create TEMPLATE_NAME ISO - create a new template from iso file
    template list - list existing templates

    run machines SPECLINE - run machines from specline (see doc). Multiple machines arg can be specified
    run file FILENAME - run machines from context file. Each file line is a specfile
      common run options :
      add 'gui' option to enable graphical interface to launched vms
      add 'foreground' option to enable running in foreground (use for daemons)

    stop - will stop running machines in the context

    destroy - will stop running machines in the context and will destroy the context and all its machines

    snap take VM_NAME - take a snapshot for VM_NAME
    snap list VM_NAME - list snapshots for VM_NAME
    snap restore VM_NAME tag TAG - restore VM_NAME from TAG snapshot
    snap delete VM_NAME tag TAG - delete TAG snapshot for VM_NAME machine

    inventory - will list running machines in the context
      note :
      $(basename $0) can be used as an ansible dynamic inventory

    console - connect to the machine's console

    qemu VM_NAME - connect to qemu console of machine VM_NAME
      common qemu options :
      add 'iso ISO' to mount ISO file as a cd/dvd in the machine
      add 'floppy IMG' to mount IMG file as a floppy in the machine
      if ISO or IMG equal 'none' the device is unmounted

  " >&2
  exit 1
}

realMe=$(readlink -e $0)
realDir=$(dirname ${realMe})
libDir=../lib
libName=cm_lib
utilLibName=util_lib
[ -d "${realDir}/${libDir}" ]  || usage "${libDir} directory was not found in ${realDir} : Installation problem."
[ -f "${realDir}/${libDir}/${libName}" ] && . ${realDir}/${libDir}/${libName} || usage "${libName} was not found in ${realDir}/${libDir} : Installation problem."
[ -f "${realDir}/${libDir}/${utilLibName}" ] && . ${realDir}/${libDir}/${utilLibName} || usage "${utilLibName} was not found in ${realDir}/${libDir} : Installation problem."

context="${vmDefaultContext}"
[ -n "${NC_CONTEXT}" ] && context="${NC_CONTEXT}"
[ ${context} == "all" ] && context="*"

[ $# -eq 0 ] && usage no argument given
command=$1
shift

case $command in
  template)
    [ $# -eq 0 ] && usage no argument given to template command
    sub_command=$1
    shift
    case $sub_command in
      download)
        vmTemplate=$1
        template_download ${vmTemplate}
        ;;
      create)
        vmTemplate=$1
        isoName=$2
        vm_create ${vmTemplate} ${vmCreateContext} "" ${isoName}
        exit
        ;;
      list)
        (cd ${vmBaseDir}/${vmCreateContext} ; ls -1 )
        exit
        ;;
      *) usage unknown argument given to template command ;;
    esac
    ;;
  run)
    run "$@"
    exit
    ;;
  stop)
    stop_all ${context}
    exit
    ;;
  destroy)
    stop_all ${context}
    vm_context_destroy ${context}
    exit
    ;;
  snap)
    vm_snapshot_manage ${context} $@
    exit
    ;;
  inventory)
    vm_running "${context}" "${typeName}"
    exit
    ;;
  --list)
    ansible_inventory "${context}" "*"
    exit
    ;;
  console)
    console $@
    exit
    ;;
  qemu)
    manage $@
    exit
    ;;
  help) usage ;;
  version) version ;;
  *) usage unknown option ${first_arg} ;;
esac

usage something went wrong
